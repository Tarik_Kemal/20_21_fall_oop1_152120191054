#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {

	vector<int> myvector;
	stringstream s(str);
	int a;
	char ch;
	while (s >> a)
	{
		myvector.push_back(a);
		s >> ch;
	}


	return myvector;

}

int main() {
	string str;
	cin >> str;
	vector<int> integers = parseInts(str);
	for (int i = 0; i < integers.size(); i++) {
		cout << integers[i] << "\n";
	}

	return 0;
}